import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions } from "@ionic-native/in-app-browser";
import { Device } from '@ionic-native/device';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';
import { Insomnia } from '@ionic-native/insomnia';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  url: string;
  sn: string;

  constructor(public navCtrl: NavController, private inAppBrowser: InAppBrowser,
              private device: Device, private androidFullScreen: AndroidFullScreen,
              private insomnia: Insomnia) { 

    this.insomnia.keepAwake()
      .then(
        () => console.log('success'),
        () => console.log('error')
      );

    this.androidFullScreen.isImmersiveModeSupported()
      .then(() => console.log('Immersive mode supported'))
      .catch(err => console.log(err));

    const options: InAppBrowserOptions = {
      location: 'no',
      zoom: 'no'
    }
    console.log('Device UUID is: ' + this.device.uuid);
    const url = "https://signgaze.com/#/board/" + this.device.uuid;
    this.sn = this.device.uuid;
    const browser = this.inAppBrowser.create(url, '_self', options);

  }
}
